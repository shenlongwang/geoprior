# Geographic Prior Generation 

This repo is the folder for Geographic Prior Code.


The geographic prior was initially described in a our CVPR 2015 paper. This tool could help render out semantic labels and depth map with a given obj file generated from OpenStreetMap.

### Citing

If you find it useful in your research, please consider citing:

    @inproceedings{wangCVPR15,
      title={Holistic 3d scene understanding from a single geo-tagged image},
      author={Wang, Shenlong and Fidler, Sanja and Urtasun, Raquel},
      booktitle = {Computer Vision and Pattern Recognition},
      year = {2015}
    }


## Code directory

Check out the readme for each subfolder

- renderer 
- tinyobjloader 

## Dependencies
- [glew](http://glew.sourceforge.net/)
- [GLE](https://gna.org/projects/gle)
- [glut](https://www.opengl.org/resources/libraries/glut/)
- [GLM](http://glm.g-truc.net/)
- [tinyobjloader](included in this repo)

## Tools we might need later for dataset visualization
- [MATLAB](https://www.mathworks.com/products/matlab.html)

## Compiling 

### Linux
- Install dependencies through `apt-get`
- Modify `TINYOBJLOADER_HOME` and `NVIDIA_LIB` in `Makefile`
- Run `make all`

### Mac 
- Install dependencies through `brew`
- Modify `TINYOBJLOADER_HOME` and `NVIDIA_LIB` in `Makefile.mac`
- Copy `Makefile.mac` to replace the original makefile
- Run `make all`


## Running 

### Command 
- Please run `./OSMrender [SeqID]` for test. For example: `./OSMrender 1`
- Before running please make sure you can create the following output folder structure: 

    * GEOPRIORROOT/output
    * ----/depth
    * --------/0000
    * --------/0001
    * ....
    * ----/label
    * ----/normal

### Input/Output Structure:

- `GEOPRIORROOT/renderer/data` The input data. You can download examples from here: [data](http://www.cs.toronto.edu/~slwang/geoprior_data.tar.gz) 
- `GEOPRIORROOT/renderer/output` The output data. 
- Semantic label is encoded with standard palette that we used for KITTI semantic images. 
- Depth is encoded as (R*65535+G*255+B)/65535.