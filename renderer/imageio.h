#include "png++/png.hpp"
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <iostream>
#include <sstream>
#include <fstream>


int testimageio()
{
    png::image< png::rgb_pixel > image(128, 128);
    for (size_t y = 0; y < image.get_height(); ++y)
    {
        for (size_t x = 0; x < image.get_width(); ++x)
        {
           image[y][x] = png::rgb_pixel(x, y, x + y);
           // non-checking equivalent of image.set_pixel(x, y, ...);
         }
    }
    image.write("rgb.png");
    
    png::image< png::rgb_pixel > image_read("rgb.png"); 
    for (size_t y = 0; y < image.get_height(); ++y)
    {
        for (size_t x = 0; x < image.get_width(); ++x)
        {
           std::cout<< int(image_read[y][x].red) << " ";
           // non-checking equivalent of image.set_pixel(x, y, ...);
        }
        std::cout<<std::endl;
    }
    
    return 0;
}
