#include <cstdio>
#include <string>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <iostream>
#include <sstream>
#include <fstream>
#include "GL/glew.h"
#include "GL/gl.h"
#include "GL/glu.h"
#include "GL/glut.h"
#include "util.h"


struct CameraParams
{
    double lat;
    double lon;
    double zNear;
    double zFar;
    double focus;
    double fov;
    double pan;
    std::vector<double> K;
    std::vector<double> R;
    unsigned int imgHeight;
    unsigned int imgWidth;
    CameraParams(): zNear(5.0), zFar(1000.0), focus(50), imgHeight(375), imgWidth(1241)
    {
        fov = focus / double(imgHeight); 
        K.resize(9);
        R.resize(9);
    }
};

typedef struct CameraParams Camera;

int CameraSetup(double zNear, double zFar, double *intrinsics, unsigned int imgHeight, unsigned int imgWidth) 
{
  
  double viewMat[] = {1, 0, 0, 0, 0, -1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 1};
  double fcv[] = {intrinsics[0], intrinsics[1]};
  double ccv[] = {intrinsics[2], intrinsics[3]};
  
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
  glEnable(GL_DEPTH_TEST);
  glDisable(GL_TEXTURE_2D);
  
  glMatrixMode(GL_MODELVIEW);
  glLoadMatrixd(viewMat);
  
  double left = - ccv[0] / fcv[0] * zNear;
  double bottom = (ccv[1] - (double)(imgHeight-1)) / fcv[1] * zNear;
  double right = ((double)imgWidth - 1.0 - ccv[0]) / fcv[0] * zNear;
  double top = ccv[1] / fcv[1] * zNear;
  
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glFrustum(left, right, bottom, top, zNear, zFar);
  glViewport(0, 0, imgWidth, imgHeight);
  return 0;
}

int CameraParse(std::string filename, int framenum, struct CameraParams *cameraParams)
{
    std::ifstream cameraFile(filename, std::ios_base::in);
    std::vector<double> inputValues;
    inputValues.resize(22);
    if (cameraFile.is_open())
    {
        GotoLine(cameraFile, framenum);
        for (size_t i = 0; i < 22; i++)
        {
            cameraFile >> inputValues[i];
        }
    }
    else
    {
        std::cerr << "File cannot open" << std::endl;
        return false;
    }
    cameraParams->lat = inputValues[2];
    cameraParams->lon = inputValues[3];
    
    for (size_t i = 0; i < 9; i++)
        cameraParams->R[i] = inputValues[i+4];
    for (size_t i = 0; i < 9; i++)
        cameraParams->K[i] = inputValues[i+13];

    if (printresult)
    {
        std::cout<< "Lat: "<< cameraParams->lat << ", Lon: "<< cameraParams->lon << std::endl; 
        std::cout<< "R: ";
        for (size_t i = 0; i < 9; i++)
            std::cout << cameraParams->R[i] << " "; 
        std::cout<< std::endl; 
        std::cout<< "K: "; 
        for (size_t i = 0; i < 9; i++)
            std::cout << cameraParams->K[i] << " "; 
        std::cout<< std::endl; 
        std::cout<< "Center Lat: "<< originLat << ", Center Lon: "<< originLon << std::endl; 
    }
    
    return 1;
}

int MapParse(std::string filename, int seqnum)
{
    std::ifstream mapFile(filename, std::ios_base::in);
    std::vector<double> inputValues;
    inputValues.resize(3);
    if (mapFile.is_open())
    {
        GotoLine(mapFile, seqnum);
        for (size_t i = 0; i < 3; i++)
        {
            mapFile >> inputValues[i];
        }
    }
    else
    {
        std::cerr << "File cannot open" << std::endl;
        return false;
    }
    originLat = inputValues[0];
    originLon = inputValues[1];
    setOrigin();

    if (printresult)
    {
        std::cout<< "Center Lat: "<< originLat << ", Center Lon: "<< originLon << std::endl; 
    }
    
    return 1;
}
