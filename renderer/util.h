#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <iostream>
#include <sstream>
#include <fstream>

bool printresult = true;

std::ifstream& GotoLine(std::ifstream& file, unsigned int num)
{
    file.seekg(std::ios::beg);
    for(size_t i=0; i < num; ++i){
        file.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
    }
    return file;
}

int LineNum(std::string filename)
{
    std::ifstream cameraFile(filename, std::ios_base::in);
    int numLines = 0;
    std::string line;
    while (std::getline(cameraFile, line))
        ++numLines;
    std::cout << "Number of lines in text file: " << numLines;
    return numLines;
}
