#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <iostream>
#include <sstream>
#include <fstream>

#define R_MAJOR 6378137.0
#define R_MINOR 6356752.3142
#define EARTH_CIRCUMFERENCE 40075016.686

const double RATIO = R_MINOR / R_MAJOR;
const double ECCENT = sqrt(1.0 - (RATIO * RATIO));
const double COM = 0.5 * ECCENT;

double degToRad(double d) { return ((d)*M_PI)/180.0;}
double radToDeg(double d) { return ((d)*180.0)/M_PI;}

double originX;                 // Origin point coordinate x in [0, 1] mercutor coordinate
double originZ;                 // Origin point coordinate x in [0, 1] mercutor coordinate

double originLat;               // Origin point coordinate x in [0, 1] mercutor coordinate
double originLon;               // Origin point coordinate x in [0, 1] mercutor coordinate

double scaleFactor;             // Scale Factor for this map, given origin lat

// Caculate earth circumference at given latitude
double earthCircumference(double lat)
{
    return EARTH_CIRCUMFERENCE * cos(degToRad(lat)); 
}


// Covert longitude to Mercator projection ([0, 1])

double lonToX(double lon)
{
    return (lon + 180.0) / 360.0;
}

double xToLon(double x)
{
    return 360.0 * (x - 0.5);
}

double latToZ(double lat)
{
    double sinLat = sin(degToRad(lat));
    return log((1.0 + sinLat) / (1.0 - sinLat)) / (4.0 * M_PI) + 0.5;
}

double zToLat(double z)
{
    return 360.0 * atan(exp((z - 0.5) * (2.0 * M_PI))) / M_PI - 90.0;
}

double latToYEllipitical(double lat)
{
    lat = fmin(89.5, fmax(lat, -89.5));
    double phi = degToRad(lat);
    double sinphi = sin(phi);
    double con = ECCENT * sinphi;
    con = pow(((1.0 - con) / (1.0 + con)), COM);
    double ts = tan(0.5 * ((M_PI * 0.5) - phi)) / con;
    return 0 - R_MAJOR * log(ts);
}

double zToLatEllipitical(double z)
{
    double ts = exp(-z / R_MAJOR);
    double phi = M_PI / 2.0 - 2 * atan(ts);
    double dphi = 1.0;
    int i = 0;
    while ((abs(dphi) > 0.000000001) && (i < 15))
    {
        double con = ECCENT * sin(phi);
        dphi = M_PI / 2.0 - 2 * atan(ts * pow((1.0 - con) / (1.0 + con), COM)) - phi;
        phi += dphi;
        i++;
    }
    return radToDeg(phi);
}

void calPos(double lat, double lon, double &mx, double &mz)
{
    mx = lonToX(lon) * scaleFactor - originX;
    mz = latToZ(lat) * scaleFactor - originZ;
    mx = round(mx * 1000) / 1000.0;
    mz = - round(mz * 1000) / 1000.0;
}

void setOrigin()
{
    scaleFactor = earthCircumference(originLat);
    originZ = latToZ(originLat) * scaleFactor;
    originX = lonToX(originLon) * scaleFactor;
}


