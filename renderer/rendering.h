#include <cstdio>
#include <cmath>
// #include <cstdlib>
#include <cassert>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <vector>
#include "GL/glew.h"
#include "GL/gl.h"
#include "GL/glu.h"
#include "GL/glut.h"
#include <glm/vec3.hpp>
#include <glm/glm.hpp>
#include <unistd.h>     // Header file for sleeping.
#include "tiny_obj_loader.h"

/* ascii code for the escape key */
#define ESCAPE 27
#define ESCAPE 27
#define PAGE_UP 73
#define PAGE_DOWN 81
#define UP_ARROW 72
#define DOWN_ARROW 80
#define LEFT_ARROW 75
#define RIGHT_ARROW 77

bool flagAuto = true;
bool flagSave = true;
bool flagMove= true;

float angle = 0.0f; // store mouse angle 
float deltaAngle = 0.0f; // store mouse angle 
int xOrigin = -1; // store the X position where the mouse is click 

double lx=0.0f,lz=-1.0f;
// XZ position of the camera
double x=0.0, z=0.0;
// the key states. These variables will be zero
bool flag_depth = false;
int height=375, width=1242;
int frame=0; // Frame Index
int seq=0;  // Sequence Index

int numFrames = 0;

char sequenceId[5]; 
char frameId[7]; 

GLfloat lookupdown = 0.0;

std::string cameraFileName;
std::string mapFileName;
GLfloat LightAmbient[]= { 0.5f, 0.5f, 0.5f, 1.0f };                 // Ambient Light Values ( NEW )


// const float lat2x = 0.0174532925f;
// const float lon2z = 0.0174532925f;


std::vector<tinyobj::shape_t> shapes;
struct CameraParams camera;

/* The number of our GLUT window */
int window; 

// double lat2z(const double lat) 
// {
//     double temp_z = lat2y_m(lat) - lat2y_m(lat_mu);
//     return -temp_z;
// }
// 
// double lon2x(const double lon) 
// {
//     std::cout << lon2x_m(lon) << ", " << lon2x_m(lon_mu) << std::endl;
//     double temp_x = lon2x_m(lon - lon_mu);
//     printf("%2.14f\n", temp_x);
//     return temp_x;
// }

// GLdouble lat2z(const GLdouble lat) 
// {
//     GLdouble z =  measure(lat, lon_mu, lat_mu, lon_mu);
//     if (lat > lat_mu)
//         z = -z;
//     return z;
// }
// 
// GLdouble lon2x(const GLdouble lon) 
// {
//     GLdouble x =  measure(lat_mu, lon, lat_mu, lon_mu);
//     if (lon < lon_mu)
//         x = -x;
//     return x;
// }

// GLdouble lat2z(const GLdouble lat) 
// {
//     GLdouble z =  -(lat - lat_mu) / 0.000008982583953;
//     return z;
// }
// 
// GLdouble lon2x(const GLdouble lon) 
// {
//     GLdouble x =  (lon - lon_mu) / 0.000013695525112;
//     return x;
// }
// 
// double z2lat(const double z) 
// {
//     GLdouble lat = -0.000008982583953 * z + lat_mu;
//     return lat;
// }
// 
// double x2lon(const double x) 
// {
//     double lon = 0.000013695525112 * x + lon_mu;
//     return lon;
// }



// loads the world from a text file.
static bool SetupWorld( const char* filename, const char* basepath = NULL)
{
    std::cout << "Loading " << filename << std::endl;

    std::string err = tinyobj::LoadObj(shapes, filename, basepath);

    if (!err.empty()) {
      std::cerr << err << std::endl;
      return false;
    }
    for (size_t i = 0; i < shapes.size(); i++) 
    {
        assert((shapes[i].mesh.indices.size() % 3) == 0);
        assert((shapes[i].mesh.positions.size() % 3) == 0);
    }
    
    if (printresult) {
        PrintName(shapes);
        // PrintInfo(shapes);
    }
    
    return true;
}


void mouseButton(int button, int state, int x, int y) 
{
    // only start motion if the left button is pressed
    if (button == GLUT_LEFT_BUTTON) 
    {
    // when the button is released
        if (state == GLUT_UP) 
        {
            angle += deltaAngle;
            xOrigin = -1;
        }
        else  
        {// state = GLUT_DOWN
            xOrigin = x;
        }
     }
}

void mouseMove(int x, int y) 
{

    // this will only be true when the left button is down
    if (xOrigin >= 0) 
    {

        // update deltaAngle
        deltaAngle = (x - xOrigin) * 0.001f;

        // update camera's direction
        lx = sin(angle + deltaAngle);
        lz = -cos(angle + deltaAngle);
    }
}

int saveToImage(std::string filename, int type)
{
    GLubyte *rgbmap; 
    GLubyte *redimage; 
    GLfloat *depthmap;
    GLfloat singledepth;
    GLubyte pixel[3] = {0, 0, 0};
    width = glutGet(GLUT_WINDOW_WIDTH);
    height = glutGet(GLUT_WINDOW_HEIGHT);
    switch(type)
    {
        case 0:
            depthmap = new GLfloat[width*height]; 
            glReadPixels(0, 0, width, height, GL_DEPTH_COMPONENT, GL_FLOAT, depthmap);
            std::cout << "Saving Depth Map..." << std::endl;
            break;
        case 1:
            rgbmap = new GLubyte[width*height*3];
            // glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
            // glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, rgbmap);
            std::cout << "Saving RGB Image..." << std::endl;
            break;
        case 2:
        {
            rgbmap = new GLubyte[(width+1)*(height+1)*3];
            glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
            glReadPixels(0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, rgbmap);
            std::cout << "Saving RGB Image..." << std::endl;
            std::ofstream myFile(filename, std::ios::out | std::ios::binary);
            myFile.write(reinterpret_cast<const char*>(rgbmap), width*height*3);
            break;
        }
        case 3:
            std::cout << "Saving Red Image..." << width << ", " << height << std::endl;
            redimage = new GLubyte[width*height];
            glReadBuffer(GL_COLOR_ATTACHMENT0);
            glReadPixels(0, 0, width, height, GL_RED, GL_UNSIGNED_BYTE, redimage);
            break;
    }
    png::image< png::rgb_pixel > image(width, height);
    for (size_t y = 0; y < image.get_height(); ++y)
    {
        for (size_t x = 0; x < image.get_width(); ++x)
        {
           // glReadPixels(x, y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);
           switch(type)
           {
               case 0:
                    // glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, pixel);
                    singledepth = depthmap[y * width + x]*255.0; 
                    pixel[0] = GLubyte(singledepth);
                    singledepth = (singledepth - floor(singledepth))*255.0; 
                    pixel[1] = GLubyte(singledepth);
                    singledepth = (singledepth - floor(singledepth))*255.0; 
                    pixel[2] = GLubyte(singledepth);
                    image[height - y - 1][x] = png::rgb_pixel(pixel[0], pixel[1], pixel[2]);
                    break;
               case 1:
                    glReadPixels(x, y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);
                    image[height - y - 1][x] = png::rgb_pixel(pixel[0], pixel[1], pixel[2]);
                    break;
               case 3:
                    // glReadPixels(x, y, 1, 1, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, pixel);
                    pixel[0] = redimage[y * width + x]; 
                    image[height - y - 1][x] = png::rgb_pixel(pixel[0], pixel[0], pixel[0]);
                    break;
               default: 
                    break;
           }
           // image[height - y - 1][x] = png::rgb_pixel(pixel[0], pixel[1], pixel[2]);
           if (false)
               std::cout << int(pixel[0]) << " " << int(pixel[1]) << " " << int(pixel[2]) << std::endl;
           // non-checking equivalent of image.set_pixel(x, y, ...);
        }
    }
    if (type!=2)
        image.write(filename);
    std::cout << "Successful!" << std::endl;
    // switch(type)
    // {
    //     case 0:
    //         delete[] depthmap; 
    //         break;
    //     case 1:
    //         delete[] rgbmap;
    //         break;
    // }
    return 1;
}

/* A general OpenGL initialization function.  Sets all of the initial parameters. */
void InitGL(int Width, int Height)	        // We call this right after our OpenGL window is created.
{
    glClearColor(0.5f, 0.5f, 0.5f, 0.0f);		// This Will Clear The Background Color To Black
    glClearDepth(1.0);				// Enables Clearing Of The Depth Buffer
    glDepthFunc(GL_LESS);				// The Type Of Depth Test To Do
    glEnable(GL_DEPTH_TEST);			// Enables Depth Testing
    glShadeModel(GL_SMOOTH);			// Enables Smooth Color Shading

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();				// Reset The Projection Matrix

    gluPerspective(29.1349f,(GLfloat)Width/(GLfloat)Height,1.0f,200.0f);	// Calculate The Aspect Ratio Of The Window
    // gluLookAt(0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);	// Calculate The Aspect Ratio Of The Window
    gluLookAt(x, 1.65f, z, x+lx, 1.65f,  z+lz, 0.0f, 1.0f,  0.0f);
    // gluLookAt(0.0f, 500.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f);	// Calculate The Aspect Ratio Of The Window

    glMatrixMode(GL_MODELVIEW);
}

/* The function called when our window is resized (which shouldn't happen, because we're fullscreen) */
void ReSizeGLScene(int Width, int Height)
{
  if (Height==0)				// Prevent A Divide By Zero If The Window Is Too Small
    Height=1;

  glViewport(0, 0, Width, Height);		// Reset The Current Viewport And Perspective Transformation

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  gluPerspective(29.1349f,(GLfloat)Width/(GLfloat)Height,1.0f,200.0f);
  // gluPerspective(45.0f,(GLfloat)Width/(GLfloat)Height,0.1f,100.0f);
  glMatrixMode(GL_MODELVIEW);
}


/* The main drawing function. */
void DrawGLScene()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// Clear The Screen And The Depth Buffer
    glLoadIdentity();				// Reset The View
    gluLookAt(x, 1.65, z, x+lx, 1.65,  z+lz, 0.0, 1.0,  0.0);
    size_t ind;
    GLfloat x_m, y_m, z_m;
    std::vector<GLfloat> shapeColor(3, 0.5f);
   
    if (flagAuto && flagMove)
    {
        frame = frame + 1;
        if (frame >= numFrames)
            frame = 0;
        CameraParse(cameraFileName, frame, &camera);
        calPos(camera.lat, camera.lon, x, z);
        lx = camera.R[0];
        lz = -camera.R[1];
        if (printresult)
        {
            std::cout << "Frame: " << frame << "/" << numFrames << std::endl;
            printf("lat %2.10f, lon %2.10f\n", camera.lat, camera.lon);
            std::cout << "x: "<<x<< ", z: "<<z<<" "<< camera.lat << " "<<camera.lon << " " << xToLon(x) << " " << zToLat(z) << std::endl;
	        // saveToImage("Capture-depth.png", 0); 
        }
        flagMove = false;
    }

    for (size_t i = 0; i < shapes.size(); i++) 
    {
        GLfloat offset_y = 0.0f;     
        shapeColor = {0.75f, 0.75f, 0.0f};
        if (shapes[i].name.find("Building")!= std::string::npos)
            shapeColor = {0.5f, 0.0f, 0.0f};
        if (shapes[i].name.find("Road")!= std::string::npos)
        {
            shapeColor = {0.5f, 0.25f, 0.5f};
            offset_y = 0.05f;
        }
        if (shapes[i].name.find("Tree")!= std::string::npos)
            shapeColor = {0.5f, 0.5f, 0.0f};
        if (shapes[i].name.find("Surface")!= std::string::npos)
            shapeColor = {0.0f, 0.0f, 0.75f};
        // std::cout << shapeColor[0] << " " << shapeColor[1] <<" " << shapeColor[2] <<std::endl;
        if (flag_depth)
        {
            glBegin(GL_QUADS);
            glVertex3f(1000.0,0.0,1000.0);
            glVertex3f(1000.0,0.0,-1000.0);
            glVertex3f(-1000.0,0.0,-1000.0);
            glVertex3f(-1000.0,0.0,1000.0);
            glColor3f( 0, 0.5, 0);
            glEnd();
            for (size_t f = 0; f < shapes[i].mesh.indices.size()/3; f++) 
            {
                ind = shapes[i].mesh.indices[3*f];
                x_m = shapes[i].mesh.positions[3*ind+0];
                y_m = shapes[i].mesh.positions[3*ind+1] + offset_y;
                if (y_m == 7.5)
                    y_m = 15;
                z_m = shapes[i].mesh.positions[3*ind+2];
                glm::vec3 v1(x_m, y_m, z_m);            
                
                ind = shapes[i].mesh.indices[3*f+1];
                x_m = shapes[i].mesh.positions[3*ind+0];
                y_m = shapes[i].mesh.positions[3*ind+1] + offset_y;
                if (y_m == 7.5)
                    y_m = 15;
                z_m = shapes[i].mesh.positions[3*ind+2];
                glm::vec3 v2(x_m, y_m, z_m);            
                
                ind = shapes[i].mesh.indices[3*f+2];
                x_m = shapes[i].mesh.positions[3*ind+0];
                y_m = shapes[i].mesh.positions[3*ind+1] + offset_y;
                if (y_m == 7.5)
                    y_m = 15;
                z_m = shapes[i].mesh.positions[3*ind+2];
                glm::vec3 v3(x_m, y_m, z_m);    
                
                glm::vec3 normal = glm::normalize(glm::cross(v3-v1,v2-v1));
                normal = {glm::dot(normal, glm::vec3({-lz, 0, lx})), normal[1], glm::dot(normal, glm::vec3({lx, 0, lz}))};
                normal = {fabs(normal[0]), fabs(normal[1]), fabs(normal[2])};
                normal = (normal + glm::vec3(1.0f, 1.0f, 1.0f))/2.0f;
                glBegin(GL_TRIANGLES);		
                glColor3f( normal[0], normal[1], normal[2]);
                glVertex3f(v1[0],v1[1],v1[2]);
                glColor3f( normal[0], normal[1], normal[2]);
                glVertex3f(v2[0],v2[1],v2[2]);
                glColor3f( normal[0], normal[1], normal[2]);
                glVertex3f(v3[0],v3[1],v3[2]);
                glEnd();	
            }
        }
        else
        {
            glBegin(GL_QUADS);
            glVertex3f(1000.0,0.0,1000.0);
            glVertex3f(1000.0,0.0,-1000.0);
            glVertex3f(-1000.0,0.0,-1000.0);
            glVertex3f(-1000.0,0.0,1000.0);
            glColor3f( 0, 1.0, 0);
            glEnd();
        
            for (size_t f = 0; f < shapes[i].mesh.indices.size()/3; f++) 
            {
                glBegin(GL_TRIANGLES);		
                glColor3f( shapeColor[0], shapeColor[1], shapeColor[2]);
                glNormal3f( 0.0f, 0.0f, 1.0f);
                ind = shapes[i].mesh.indices[3*f];
                x_m = shapes[i].mesh.positions[3*ind+0];
                y_m = shapes[i].mesh.positions[3*ind+1] + offset_y;
                if (y_m == 7.5)
                    y_m = 15;
                z_m = shapes[i].mesh.positions[3*ind+2];
                glVertex3f(x_m,y_m,z_m);
                
                ind = shapes[i].mesh.indices[3*f+1];
                x_m = shapes[i].mesh.positions[3*ind+0];
                y_m = shapes[i].mesh.positions[3*ind+1] + offset_y;
                if (y_m == 7.5)
                    y_m = 15;
                z_m = shapes[i].mesh.positions[3*ind+2];
                glVertex3f(x_m,y_m,z_m);
                
                ind = shapes[i].mesh.indices[3*f+2];
                x_m = shapes[i].mesh.positions[3*ind+0];
                y_m = shapes[i].mesh.positions[3*ind+1] + offset_y;
                if (y_m == 7.5)
                    y_m = 15;
                z_m = shapes[i].mesh.positions[3*ind+2];
                glVertex3f(x_m,y_m,z_m);
                glEnd();	
            }
        }
    } 
    
    glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient);             // Setup The Ambient Light
    // since this is double buffered, swap the buffers to display what just got drawn.
    glutSwapBuffers();
    
    if (flagAuto && !flagMove)
    {
        flagMove = true;
        
        if (flagSave)
        {
            sprintf(frameId, "%06d", frame - 1);
            std::ostringstream depthStream;
            depthStream << "./output/depth/" << sequenceId << "/" << frameId << ".png";
            std::string depthFileName = depthStream.str();
	        saveToImage(depthFileName, 0); 
       
            sprintf(frameId, "%06d", frame - 1);
            std::ostringstream labelStream;
            if (flag_depth)
                labelStream << "./output/normal/" << sequenceId << "/" << frameId << ".png";
            else
                labelStream << "./output/label/" << sequenceId << "/" << frameId << ".png";
            std::string labelFileName = labelStream.str();
	        saveToImage(labelFileName, 1); 
        
        }
    }
    
    // gluLookAt(0.0f, 500.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f);	// Calculate The Aspect Ratio Of The Window
}

/* The function called whenever a key is pressed. */
void keyPressed(unsigned char key, int keyx, int keyy) 
{
    /* avoid thrashing this procedure */
    usleep(100);

    /* If escape is pressed, kill everything. */
    if (key == ESCAPE) 
    { 
	/* shut down our window */
	glutDestroyWindow(window); 
	
	/* exit the program...normal termination. */
	exit(0);                   
    }
    if (key == 'd') 
    { 
        sprintf(frameId, "%06d", frame);
        std::ostringstream depthStream;
        depthStream << "./output/depth/" << sequenceId << "/" << frameId << ".png";
        std::string depthFileName = depthStream.str();
	    saveToImage(depthFileName, 0); 
    }
    
    if (key == 's') 
    { 
        flag_depth = !flag_depth;
    }
    
    if (key == 'l') 
    { 
        sprintf(frameId, "%06d", frame - 1);
        std::ostringstream labelStream;
        if (flag_depth)
            labelStream << "./output/normal/" << sequenceId << "/" << frameId << ".png";
        else
            labelStream << "./output/label/" << sequenceId << "/" << frameId << ".png";
        std::string labelFileName = labelStream.str();
	    saveToImage(labelFileName, 1); 
    }
    if (key == 'n') 
    {
        frame = frame + 1;
        CameraParse(cameraFileName, frame, &camera);
        // x = lon2x(camera.lon);  
        // z = lat2z(camera.lat);
        calPos(camera.lat, camera.lon, x, z);
        // update camera's direction
        lx = camera.R[0];
        lz = -camera.R[1];
        if (printresult)
        {
            std::cout << "Frame: " << frame << "/" << numFrames << std::endl;
            printf("lat %2.10f, lon %2.10f\n", camera.lat, camera.lon);
            std::cout << "x: "<<x<< ", z: "<<z<<" "<< camera.lat << " "<<camera.lon << " " << xToLon(x) << " " << zToLat(z) << std::endl;
	        // saveToImage("Capture-depth.png", 0); 
        } 
    }
    if (key == 'p') 
    {
        frame = fmax(0.0, frame - 1);
        CameraParse(cameraFileName, frame, &camera);
        // x = lon2x(camera.lon);  
        // z = lat2z(camera.lat);
        calPos(camera.lat, camera.lon, x, z);
        // update camera's direction
        lx = camera.R[0];
        lz = -camera.R[1];
        if (printresult)
        {
            std::cout << "Frame: " << frame << std::endl;
            printf("lat %2.10f, lon %2.10f\n", camera.lat, camera.lon);
            std::cout << "x: "<<x<< ", z: "<<z<<" "<< camera.lat << " "<<camera.lon << " " << xToLon(x) << " " << zToLat(z) << std::endl;
	        // saveToImage("Capture-depth.png", 0); 
        }
    }
}



int testgl(int argc, char **argv) 
{  
    /* Initialize GLUT state - glut will take any command line arguments that pertain to it or 
        X Windows - look at its documentation at http://reality.sgi.com/mjk/spec3/spec3.html */  
    glutInit(&argc, argv);  
        
    if (argc > 1)
    {
        std::ostringstream shapeStream;
        seq = atoi(argv[1])-1;
        sprintf(sequenceId, "%04d", seq);
        // shapeStream << "./data/shape/kittisequence" << argv[1] << ".obj"; 
        shapeStream << "./data/obj/" << sequenceId << ".obj"; 
        std::string shapeFileName = shapeStream.str();
        
        std::ostringstream cameraStream;
        cameraStream << "./data/camera/extrinsc/" << sequenceId << ".txt";
        cameraFileName = cameraStream.str();
        numFrames = LineNum(cameraFileName);
        mapFileName = "./data/obj/mapcenter.txt"; 
        if (printresult)
        {
            std::cout << "CameraFile: "<< cameraFileName << std::endl;
            std::cout << "ShapeFile: "<< shapeFileName << std::endl;
            std::cout << "MapFile: "<< mapFileName << std::endl;

        }
        
        SetupWorld(shapeFileName.c_str()); 
        MapParse(mapFileName, seq);
        CameraParse(cameraFileName, frame, &camera);
        // x = lon2x(camera.lon);  
        // z = lat2z(camera.lat);
        calPos(camera.lat, camera.lon, x, z);
    }
    else
    {
        std::cout << "Not enough arguments. Please provide filename of your shape objects and camera parameters. " << std::endl;
        return 0;
    }
    /* Select type of Display mode:   
       Double buffer 
       RGBA color
       Alpha components supported 
       Depth buffer */  
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH);  

    /* get a 640 x 480 window */
    glutInitWindowSize(width, height);  

    /* the window starts at the upper left corner of the screen */
    glutInitWindowPosition(20, 20);  

    /* Open a window */  
    window = glutCreateWindow("Test GL");  
    
    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
      /* Problem: glewInit failed, something is seriously wrong. */
        fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
    }
    fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));

    /* Register the function to do all our OpenGL drawing. */
    glutDisplayFunc(&DrawGLScene);  

    /* Go fullscreen.  This is as soon as possible. */
    // glutFullScreen();

    /* Even if there are no events, redraw our gl scene. */
    glutIdleFunc(&DrawGLScene);

    /* Register the function called when our window is resized. */
    glutReshapeFunc(&ReSizeGLScene);

    /* Register the function called when the keyboard is pressed. */
    glutKeyboardFunc(&keyPressed);

    glutMouseFunc(mouseButton);
    
    glutMotionFunc(mouseMove);

    /* Initialize our window. */
    InitGL(width, height);
    
    /* Start Event Processing Engine */  
    glutMainLoop();  

    return 1;
}
